export interface FormState {
  pass: string;
  checkPass: string;
  oldPass: string;
}
export type Validation = {
  trigger?: string;
  message?: string;
  validator?: any;
  required?: boolean;
  pattern?: RegExp;
};
export type Errors = {
  errors: Array<string>;
  name: Array<string>;
  warnings: Array<string>;
};

export type Rules = {
  pass: Validation[];
  checkPass: Validation[];
};

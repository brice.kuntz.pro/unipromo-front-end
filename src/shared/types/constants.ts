interface IStatus {
  color: string;
  title: string;
}

export const STATUSES: Record<string, IStatus> = {
  1: {
    color: 'var(--main-purple)',
    title: 'A venir',
  },
  2: {
    color: 'var(--main-green)',
    title: 'En cours',
  },
  3: {
    color: 'var(--light-gray)',
    title: 'Terminé',
  },
};

interface IColumn {
  key: string;
  title: string;
}

export type TColumns = Array<IColumn>;

import { reactive, ref } from 'vue';
import type {
  Validation,
  FormState,
  Rules,
} from '@/shared/types/passwordValidator.types';
export function passwordValidator() {
  const errors = ref<Array<string>>([]);

  const formState: FormState = reactive({
    pass: '',
    checkPass: '',
    oldPass: '',
  });
  const checkPasswordsMatch = async (_rule: any, value: string) => {
    if (value !== formState.pass) {
      return Promise.reject('Les mots de passe ne correspondent pas');
    } else {
      return Promise.resolve();
    }
  };
  const customValidations: Array<Validation> = reactive([
    {
      pattern: /^.{8,}$/,
      message: 'plus de 8 caractères',
      trigger: 'blur',
    },
    {
      pattern: /\d/,
      message: 'au moins un chiffre',
      trigger: 'blur',
    },
    {
      pattern: /(?=.*[a-z])(?=.*[A-Z])/,
      message: 'lettres majuscules et minuscules',
      trigger: 'blur',
    },
  ]);

  const customRules: Rules = {
    pass: [
      ...customValidations,
      {
        required: true,
        trigger: 'change',
        message: 'Veuiller saisir tous les champs obligatoires',
      },
    ],
    checkPass: [
      { validator: checkPasswordsMatch, trigger: 'blur' },
      {
        required: true,
        message: 'Veuiller saisir tous les champs obligatoires',
      },
    ],
  };
  return {
    customRules,
    customValidations,
    errors,
    formState,
    checkPasswordsMatch,
  };
}

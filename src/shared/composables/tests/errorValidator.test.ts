import { errorValidator } from '@/shared/composables/errorValidator';
import { test, expect, describe } from 'vitest';
import type {
  Errors,
  Validation,
} from '@/shared/types/passwordValidator.types';
describe('errorValidator', () => {
  test('get error should return empty array', async () => {
    const data: Array<string> = [];
    const actual = errorValidator().getErrors(data);
    const expected: Array<string> = [];
    expect(actual).toStrictEqual(expected);
  });
  test('get error should return errors ', async () => {
    const data: Array<Errors> = [
      { errors: ['required', 'awesome'], name: [], warnings: [] },
      { errors: ['required'], name: [], warnings: [] },
    ];
    const actual = errorValidator().getErrors(data);
    const expected: Array<string> = ['required', 'awesome'];
    expect(actual).toStrictEqual(expected);
  });

  test('filter error array empty', async () => {
    const arr1: Array<string> = [];
    const arr2: Array<string> = [];
    const actual = errorValidator().filterArray(arr1, arr2);
    const expected: Array<string> = [];
    expect(actual).toStrictEqual(expected);
  });
  test('filter error array', async () => {
    const arr1: Array<string> = ['required', 'hello', 'awesome'];
    const arr2: Array<string> = ['required'];
    const actual = errorValidator().filterArray(arr1, arr2);
    const expected: Array<string> = ['hello', 'awesome'];
    expect(actual).toStrictEqual(expected);
  });
  test('filter error array', async () => {
    const arr1: Array<string> = ['required', 'hello', 'awesome'];
    const arr2: Array<string> = [];
    const actual = errorValidator().filterArray(arr1, arr2);
    const expected: Array<string> = ['required', 'hello', 'awesome'];
    expect(actual).toStrictEqual(expected);
  });

  test('get error messages should return empty array', async () => {
    const data: Array<string> = [];
    const actual = errorValidator().getErrorMessages(data);
    const expected: Array<string> = [];
    expect(actual).toStrictEqual(expected);
  });
  test('get error messages should return errors ', async () => {
    const data: Array<Validation> = [
      { message: 'message1', trigger: 'change' },
      { required: true, message: 'message2' },
    ];
    const actual = errorValidator().getErrorMessages(data);
    const expected: Array<string> = ['message1', 'message2'];
    expect(actual).toStrictEqual(expected);
  });
});

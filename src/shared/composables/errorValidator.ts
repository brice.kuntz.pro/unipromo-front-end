export function errorValidator() {
  const getErrors = (values: Array<any>): Array<string> => {
    if (values.length === 0) {
      return [];
    } else {
      const filteredArray: Array<string> = values.map((el) => el.errors).flat();
      return [...new Set(filteredArray)];
    }
  };
  const filterArray = (
    array1: Array<string>,
    array2: Array<string | undefined>
  ): Array<string> => {
    return array1.filter((val: string) => !array2.includes(val));
  };
  const getErrorMessages = (arr: Array<any>): Array<string> => {
    return arr.map((el) => el.message);
  };
  return {
    getErrors,
    filterArray,
    getErrorMessages,
  };
}

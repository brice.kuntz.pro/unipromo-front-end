import { createRouter, createWebHistory } from 'vue-router';
import { setupLayouts } from 'virtual:generated-layouts';
import generatedRoutes from 'virtual:generated-pages';
import { useAuth } from '@/store/modules/auth';
import authMiddleware from './middleware/auth';
const routes = setupLayouts(generatedRoutes);

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const store = useAuth();
  if (to.meta.middleware) {
    authMiddleware({ next, router, store });
  } else {
    return next();
  }
});

export default router;

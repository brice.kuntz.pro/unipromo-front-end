import { EGettersAuth } from '@/store/modules/auth';
export default function ({ next, router, store }) {
  if (!store[EGettersAuth.IS_LOGGED_IN]) {
    return router.push({ name: 'login' });
  } else {
    next();
  }
}

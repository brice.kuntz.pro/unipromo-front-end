import { defineStore } from 'pinia';
import Cookies from 'js-cookie';
import { baseApi, EActionsApi } from '../api';
export enum EAuth {
  AUTH_TOKEN = 'auth_token',
}

export enum EActionsAuth {
  SET_USER = 'SET_USER',
  SET_TOKEN = 'SET_TOKEN',
  LOGIN = 'LOGIN',
  LOGOUT = 'LOGOUT',
  FETCH_USER = 'FETCH_USER',
}

export enum EGettersAuth {
  GET_USER = 'GET_USER',
  GET_TOKEN = 'GET_TOKEN',
  IS_LOGGED_IN = 'IS_LOGGED_IN',
}

export interface ELoginData {
  email: string;
  password: string;
}

interface IUser {
  id?: number;
  email?: string;
  name?: string;
  firstName?: string;
  lastName?: string;
  temporaryPasswordChanged?: boolean;
}

const initialToken = Cookies.get(EAuth.AUTH_TOKEN)
  ? Cookies.get(EAuth.AUTH_TOKEN)
  : '';

export const useAuth = defineStore('auth', {
  state: () => {
    return {
      user: {} as IUser,
      token: initialToken as string,
    };
  },
  getters: {
    [EGettersAuth.GET_USER]: (state) => state.user,
    [EGettersAuth.GET_TOKEN]: (state) => state.token,
    [EGettersAuth.IS_LOGGED_IN]: (state) => !!state.token,
  },
  actions: {
    [EActionsAuth.SET_TOKEN](token: string) {
      this.token = token;
    },
    [EActionsAuth.SET_USER](user: IUser) {
      this.user = user;
    },
    [EActionsAuth.LOGIN](loginData: ELoginData) {
      const api = baseApi();
      return new Promise((resolve, reject) => {
        api[EActionsApi.REQUEST]<{ token: string }>({
          method: 'post',
          path: 'authentication_token',
          body: JSON.stringify(loginData),
        })
          .then(({ token }) => {
            Cookies.set(EAuth.AUTH_TOKEN, token, {
              expires: 30,
            });
            this[EActionsAuth.SET_TOKEN](token);
            this[EActionsAuth.FETCH_USER]().then(() => {
              resolve(true);
            });
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    [EActionsAuth.LOGOUT]() {
      return new Promise((resolve) => {
        Cookies.remove(EAuth.AUTH_TOKEN);
        this[EActionsAuth.SET_TOKEN]('');
        this[EActionsAuth.SET_USER]({});
        resolve(true);
      });
    },
    [EActionsAuth.FETCH_USER]() {
      const api = baseApi();
      return new Promise((resolve, reject) => {
        if (!this[EGettersAuth.IS_LOGGED_IN]) {
          resolve(false);
          return false;
        }
        api[EActionsApi.REQUEST]<IUser>({
          method: 'get',
          path: 'users/me',
        })
          .then((resp) => {
            resolve(resp);
            this[EActionsAuth.SET_USER](resp);
          })
          .catch((err) => {
            this[EActionsAuth.LOGOUT]();
            reject(err);
          });
      });
    },
  },
});

import { defineStore } from 'pinia';
import { v4 as uuidv4 } from 'uuid';
import { useAuth, EActionsAuth, EGettersAuth } from '../modules/auth';

const BASE_URL = 'http://unipromo-qa.noveogroup.com:8888/api';

export enum EActionsApi {
  REQUEST = 'REQUEST',
  SET_REQUEST = 'SET_REQUEST',
  DELETE_REQUEST = 'DELETE_REQUEST',
  CANCEL_REQUEST = 'CANCEL_REQUEST',
  CANCEL_ALL_REQUESTS = 'CANCEL_ALL_REQUESTS',
}

export const makeURL = (
  path: string,
  query: Record<string, string>
): string => {
  const url = new URL(`${BASE_URL}/${path}`);
  url.search = new URLSearchParams(query).toString();
  return url.toString();
};

export const baseApi = defineStore('baseApi', {
  state: () => {
    return { requests: {} as Record<string, AbortController> };
  },
  actions: {
    [EActionsApi.SET_REQUEST]({
      requestId,
      handler,
    }: {
      requestId: string;
      handler: AbortController;
    }) {
      this.requests[requestId] = handler;
    },
    [EActionsApi.DELETE_REQUEST](requestId: string): void {
      const controller: AbortController = this.requests[requestId];
      if (controller && typeof controller.abort === 'function') {
        controller.abort();
        delete this.requests[requestId];
      }
    },
    [EActionsApi.CANCEL_REQUEST](requestId: string): void | Promise<boolean> {
      if (this.requests[requestId]) {
        this[EActionsApi.DELETE_REQUEST](requestId);
      }
      return Promise.resolve(true);
    },
    [EActionsApi.CANCEL_ALL_REQUESTS](): void | Promise<boolean> {
      Object.keys(this.requests).forEach((key) => {
        this[EActionsApi.CANCEL_REQUEST](key);
      });
      return Promise.resolve(true);
    },
    async [EActionsApi.REQUEST]<T>(config): Promise<T> {
      const auth = useAuth();
      const {
        method = 'GET',
        path = '',
        body = null,
        query = {},
        headers = { 'Content-Type': 'application/json' },
        requestId = uuidv4(),
        cancel = () => {},
      } = config;

      const controller = new AbortController();
      const signal = controller.signal;
      signal.addEventListener('abort', cancel);

      try {
        this[EActionsApi.SET_REQUEST]({ requestId, handler: controller });

        const response = await fetch(makeURL(path, query), {
          method,
          headers: {
            Authorization: auth[EGettersAuth.IS_LOGGED_IN]
              ? `Bearer ${auth[EGettersAuth.GET_TOKEN]}`
              : null,
            Accept: 'application/json',
            ...headers,
          },
          body,
          signal,
        });

        if (!response.ok) return Promise.reject(response);

        let result;

        if (response.headers.get('Content-Type').includes('application/json'))
          try {
            result = await response.clone().json();
          } catch (error) {
            console.error(
              'API REQUEST ERROR. Could not convert res to JSON',
              error
            );
          }
        if (response.headers.get('Content-Type').includes('text/html'))
          try {
            result = await response.clone().text();
          } catch (error) {
            console.error(
              'API REQUEST ERROR. Could not convert res to text',
              error
            );
          }

        return Promise.resolve(result);
      } catch (error) {
        return Promise.reject(error);
      } finally {
        this[EActionsApi.DELETE_REQUEST](requestId);
      }
    },
  },
});

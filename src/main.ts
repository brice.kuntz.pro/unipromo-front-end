import { createApp } from 'vue';
import { createPinia } from 'pinia';
import Antd from 'ant-design-vue';
import { useAuth, EActionsAuth } from './store/modules/auth';
import App from './App.vue';
import 'ant-design-vue/dist/antd.css';
import './assets/styles/index.scss';
import router from './router';
const app = createApp(App);
app.use(Antd);
app.use(createPinia());
const auth = useAuth();
auth[EActionsAuth.FETCH_USER]().finally(() => {
  app.use(router);
  app.mount('#app');
});

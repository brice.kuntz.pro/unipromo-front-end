#!/bin/bash

# Disable exit on non 0
set +e
devspace purge --profile=testing --kube-context=testing --namespace="${CI_COMMIT_SHA}"
# Enable exit on non 0
set -e

devspace deploy --profile=testing --kube-context=testing --no-warn --namespace="${CI_COMMIT_SHA}"

while [[ $(kubectl get pods --context=testing --namespace="${CI_COMMIT_SHA}" -l io.kompose.service=frontend -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for frontend pod" && sleep 1; done

# run tests, linters
# devspace enter --profile=testing --kube-context=testing --namespace="${CI_COMMIT_SHA}" --container=frontend --wait make tests
devspace purge --profile=testing --kube-context=testing --namespace="${CI_COMMIT_SHA}"

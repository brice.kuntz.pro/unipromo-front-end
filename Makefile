docker_install:
	docker-compose build
	docker-compose up -d
	docker-compose exec -T frontend yarn install
	docker-compose down

docker_start:
	docker-compose build
	docker-compose up -d

docker_stop:
	docker-compose down

docker_bash:
	docker-compose exec --user=node frontend bash

# kubernetes
start:
	minikube start --driver docker --memory 2048 --cpus 2 --profile minikube
	devspace purge --kube-context=minikube --namespace=frontend-dev --no-warn
	rm -rf node_modules
	devspace dev --kube-context=minikube --namespace=frontend-dev --no-warn --ui=false --skip-push=true --print-sync=false

stop:
	- devspace purge --kube-context=minikube --namespace=frontend-dev --no-warn
	minikube stop --profile minikube

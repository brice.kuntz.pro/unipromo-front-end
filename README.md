# unipromo

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
yarn install
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

### Type-Check, Compile and Minify for Production

```sh
yarn build
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```


## Setup project using Docker environment

```sh
make docker_install
```

### Compile and Hot-Reload for Development
```sh
make docker_start
```

### stop environment
```sh
make docker_stop
```

## Setup project using Kubernetes environment

### Compile and Hot-Reload for Development
```sh
make start
```

### stop environment
```sh
make stop
```

## Gitlab ci

There is `.gitlab-ci.yml` configuration file.

The next part of configuration fixes docker host access on public gitlab runners.
```yaml
services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
```

There are 2 jobs:
* testing - it runs on each commit pushing
* deploy_staging - it deploys on each commit pushing to `main` branch

Next variables should be set in CI/CD variables https://gitlab.com/brice.kuntz.pro/unipromo-front-end/-/settings/ci_cd
* CI_REGISTRY - container registry url 
* CI_REGISTRY_PASSWORD - personal access token https://gitlab.com/-/profile/personal_access_tokens
* CI_REGISTRY_USER - username
* TESTING_URL - kubernetes cluster url for test environment
* TESTING_CLIENT_CRT_DATA - certificate data (can be received using `kubectl config view --flatten`)
* TESTING_CLIENT_KEY_DATA - key data (can be received using `kubectl config view --flatten`)
* STAGING_URL - kubernetes cluster url for staging environment
* STAGING_CLIENT_CRT_DATA - certificate data (can be received using `kubectl config view --flatten`)
* STAGING_CLIENT_KEY_DATA - key data (can be received using `kubectl config view --flatten`) 
